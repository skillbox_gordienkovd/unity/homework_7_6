using System;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    /**
     * Константы. Настройки игры.
     */
    private const int NeedFoodCountForWin = 100;

    private const int CreateFarmerNeedSeconds = 2;
    private const int CreateWarriorNeedSeconds = 2;
    private const int WaveArrivalSeconds = 10;

    /**
     * Для ежесекундного обновления
     */
    private int _nextUpdate = 1;

    /**
     * Элементы интерфейса
     */
    public Text needFoodCountForWin;
    public Text farmerCount;
    public Text warriorCount;
    public Text foodCount;
    public Text enemyWaveNumber;
    public Text enemyCount;
    public Text waveArrivalSeconds;
    public Text gameResult;
    public Text pauseOrPLayButtonText;
    public Text disableOrEnableSoundButtonText;
    public Button farmerAddButton;
    public Button warriorAddButton;
    public AudioSource seedCollected;
    public AudioSource enemyAttack;
    public AudioSource warriorSpawn;
    public AudioSource farmerSpawn;

    /**
     * Фермер
     */
    private int _createFarmerCostFood = 1;

    private int _farmerCount = 1;
    private bool _farmerCreationInProgress;
    private int _farmerCreationRemainedInSeconds = 2;

    /**
     * Воин
     */
    private int _createWarriorCostFood = 2;

    private int _warriorCount;
    private bool _warriorCreationInProgress;
    private int _warriorCreationRemainedInSeconds = 2;

    /**
     * Еда
     */
    private int _foodCount;

    /**
     * Враг
     */
    private int _enemyWaveNumber = 1;

    private int _enemyCount = 1;
    private int _enemyWaveRemainedInSeconds;

    private bool _paused;

    public void AddFarmer()
    {
        _farmerCreationInProgress = true;
        _farmerCreationRemainedInSeconds = CreateFarmerNeedSeconds;
        UpdateFoodCount(count: -_createFarmerCostFood);

        farmerAddButton.image.fillAmount = 0;
    }

    public void AddWarrior()
    {
        _warriorCreationInProgress = true;
        _warriorCreationRemainedInSeconds = CreateWarriorNeedSeconds;
        UpdateFoodCount(count: -_createWarriorCostFood);
    }

    public void PauseOrPlayGame()
    {
        _paused = !_paused;

        if (_paused)
        {
            Time.timeScale = 0;
            pauseOrPLayButtonText.text = "Играть";
        }
        else
        {
            Time.timeScale = 1;
            pauseOrPLayButtonText.text = "Пауза";
        }
    }

    public void DisableOrEnableAudioListener()
    {
        AudioListener.pause = !AudioListener.pause;

        disableOrEnableSoundButtonText.text = AudioListener.pause ? "Вкл. звук" : "Выкл. звук";
    }

    private void Start()
    {
        gameResult.enabled = false;

        _enemyWaveRemainedInSeconds = WaveArrivalSeconds;

        needFoodCountForWin.text = NeedFoodCountForWin.ToString();
        waveArrivalSeconds.text = WaveArrivalSeconds.ToString();
        farmerCount.text = _farmerCount.ToString();
        warriorCount.text = _warriorCount.ToString();
        foodCount.text = _foodCount.ToString();
        enemyWaveNumber.text = _enemyWaveNumber.ToString();
        enemyCount.text = _enemyCount.ToString();
    }

    private void Update()
    {
        if (Time.time >= _nextUpdate)
        {
            _nextUpdate = Mathf.FloorToInt(Time.time) + 1;
            UpdateEverySecond();
        }

        CheckFarmerButtonAvailability();
        CheckWarriorButtonAvailability();
    }

    private void CheckFarmerButtonAvailability()
    {
        farmerAddButton.interactable = _foodCount >= _createFarmerCostFood && !_farmerCreationInProgress;
    }

    private void CheckWarriorButtonAvailability()
    {
        warriorAddButton.interactable = _foodCount >= _createWarriorCostFood && !_warriorCreationInProgress;
    }

    private void UpdateEverySecond()
    {
        PlaySound(audioSource: seedCollected);
        UpdateFoodCount(count: _farmerCount);
        UpdateFarmerCreationState();
        UpdateWarriorCreationState();
        CheckWinState();
        CheckEnemyWave();
    }

    private static void PlaySound(AudioSource audioSource)
    {
        audioSource.Play();
    }

    private void CheckEnemyWave()
    {
        UpdateWaveArrivalSeconds(seconds: -1);

        if (_enemyWaveRemainedInSeconds != 0) return;
        
        PlaySound(audioSource: enemyAttack);

        var warriorsRemained = _warriorCount - _enemyCount;

        if (warriorsRemained < 0)
        {
            UpdateWarriorCount(count: -_warriorCount);
            UpdateFoodCount(count: -_foodCount);
            UpdateFarmerCount(count: -_farmerCount);
            DisplayLose();
        }
        else
        {
            UpdateWarriorCount(count: -_enemyCount);
            IncrementEnemyCount();
            IncrementEnemyWaveNumber();
            _enemyWaveRemainedInSeconds = WaveArrivalSeconds;
        }
    }

    private void CheckWinState()
    {
        if (_foodCount < NeedFoodCountForWin) return;

        Time.timeScale = 0;
        gameResult.text = "ПОБЕДА";
        gameResult.enabled = true;
    }

    private void DisplayLose()
    {
        Time.timeScale = 0;
        gameResult.text = "ПОРАЖЕНИЕ";
        gameResult.enabled = true;
    }

    private void UpdateFarmerCreationState()
    {
        if (!_farmerCreationInProgress) return;

        _farmerCreationRemainedInSeconds--;

        farmerAddButton.image.fillAmount = 1f - (float)_farmerCreationRemainedInSeconds / CreateFarmerNeedSeconds;

        if (_farmerCreationRemainedInSeconds != 0) return;

        UpdateFarmerCount(count: 1);
        _createFarmerCostFood = Math.Max(1, _farmerCount);
        _farmerCreationInProgress = false;
        farmerAddButton.interactable = true;
        PlaySound(audioSource: farmerSpawn);
    }

    private void UpdateWarriorCreationState()
    {
        if (!_warriorCreationInProgress) return;

        _warriorCreationRemainedInSeconds--;
        
        warriorAddButton.image.fillAmount = 1f - (float)_warriorCreationRemainedInSeconds / CreateWarriorNeedSeconds;

        if (_warriorCreationRemainedInSeconds != 0) return;

        UpdateWarriorCount(count: 1);
        _createWarriorCostFood = Math.Max(2, _warriorCount);
        _warriorCreationInProgress = false;
        warriorAddButton.interactable = true;
        PlaySound(audioSource: warriorSpawn);
    }

    private void UpdateWarriorCount(int count)
    {
        _warriorCount += count;
        warriorCount.text = _warriorCount.ToString();
    }

    private void UpdateFarmerCount(int count)
    {
        _farmerCount += count;
        farmerCount.text = _farmerCount.ToString();
    }

    private void UpdateFoodCount(int count)
    {
        _foodCount += count;
        foodCount.text = _foodCount.ToString();
    }

    private void IncrementEnemyWaveNumber()
    {
        _enemyWaveNumber++;
        enemyWaveNumber.text = _enemyWaveNumber.ToString();
    }

    private void IncrementEnemyCount()
    {
        _enemyCount++;
        enemyCount.text = _enemyCount.ToString();
    }

    private void UpdateWaveArrivalSeconds(int seconds)
    {
        _enemyWaveRemainedInSeconds += seconds;
        waveArrivalSeconds.text = _enemyWaveRemainedInSeconds.ToString();
    }
}